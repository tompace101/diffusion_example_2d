An example simulation using the `nanopore_diffusion` code.

# Folders

- logs: output from the scripts and other programs when run
- mesh: mesh data files, including templates but not parameter definition files
- modules: python code to be imported
- notebooks: jupyter notebooks, used for pre/post-processing
- postproc: post-processing outputs
- requests: request input files
- scripts: supplemental python scripts
- solutions: simulation output data

# Requests

- cleanup: remove output of the tests below
- test01: two-dimensional slot geometry without inclusions, using symmetry
- test02: two-dimensional slot geometry with two fixed inclusions, no symmetry
- test03: example of a job list: moving fixed inclusions closer together
- test04: reactive inclusions, 3 chemical species
- test05: randomized lattice inclusions (1 species, no reactive inclusions)

# Setup

The files in this repo depend on the `nanopore_diffusion` repo,
which should be added to the python path.
This can be done without creating copies of the `nanopore_diffusion` repo.
From within the `nanopore_diffusion` folder, execute:
`python setup.py develop --user`
This is only needed once.

# Usage

If using the singularity image, start it with
`singularity run fenics_2019.simg`
from the directory containing the image file,
and then change into this directory.

A script is provided here to set the necessary environment variables before running.
`source ./set_env.sh`

To run the requests in a given file:
`python -m simproc <request file>`
or, with doit:
`doit control=<request file>`

Some requests may generate other request files.
The generated request files are not run automatically.
A separate command is required to run them.
For example, to run test03, both of the following steps are required, in order:
`python -m simproc requests/mk_test03.yaml`
`python -m simproc requests/generated/test03.yaml`
There is a convenience script for this at `scripts/runjob.sh`:
`runjob.sh test03` will perform both of the actions above.

To clean up the output from a particular request file, use the `--select` argument:
`python -m simproc requests/cleanup.yaml --select clean_logs test01.yaml`

To run a jupyter notebook under singularity:
`export XDG_RUNTIME_DIR=""`
`jupyter notebook --no-browser`

# Workflow

Mesh Generation:

- a gmsh input file (.geo) is generated from a template (.geo.jinja2).
- gmsh is run to create the .msh file from the .geo file. (Other output files are also created.)
- dolfin-convert is run to create an xml file from the msh file.
- A module in `nanopore_diffusion` is called to convert the xml file to hdf5 format.

FEM Simulations:

- The LPB equation is solved to compute the potential
- The Smoluchowski equation is solved to compute concentration fields
- The data is post-processed