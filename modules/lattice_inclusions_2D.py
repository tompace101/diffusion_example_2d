"""Generate inclusions in 2D based on a lattice,
and optionally perturb their positions randomly."""

#Site packages
import numpy as np
import numpy.linalg as la
import scipy.spatial

#Functions

def generate_lattice_inclusions(bbox,incrad,minsep,fixed,idstart,idstep):
  """Create inclusions on a lattice within the given bounding box
  
  Arguments:

    - bbox = bounding box for the inclusions (xmin,ymin,xmax,ymax), all floats.
      Note that minsep is applied to the bounding box edges,
      so no generated inclusion will actually touch the bounding box.
    - incrad = inclusion radius, float.
    - minsep = minimum separation distance between inclusion surfaces (not centers)
    - fixed = list of fixed inclusions that may (or may not) be within the bounding box,
      each inclusion a tuple (ID, center x, center y, radius).
    - idstart = ID to use fo the first inclusion, as integer
    - idstep = number to increment the inclusion id by
  
  Returns:

    - generated_inclusions = list of generated inclusions,
      each inclusion a tuple as for ``fixed``

  """
  #Calculate the number of circles in each direction
  xmin,ymin,xmax,ymax=bbox
  Xspan=xmax-xmin
  Yspan=ymax-ymin
  min_lsep = 2*incrad + minsep
  min_endsep = incrad + minsep
  Nx=1+int(np.floor((Xspan-2*min_endsep)/min_lsep)) #Number of x-values for circles
  Ny=1+int(np.floor((Yspan-2*min_endsep)/min_lsep)) #Number of y-values for circles
  #Separation distances
  alpha_x = float(Xspan)/float((Nx-1)*min_lsep + 2*min_endsep)
  alpha_y = float(Yspan)/float((Ny-1)*min_lsep + 2*min_endsep)
  xsep=alpha_x*min_lsep
  ysep=alpha_y*min_lsep
  x_endsep=alpha_x*min_endsep
  y_endsep=alpha_y*min_endsep
  #X and Y coordinates of lattice points
  xcoords=[xmin+x_endsep + xsep*float(ii) for ii in range(Nx)]
  ycoords=[ymin+y_endsep + ysep*float(jj) for jj in range(Ny)]
  #Matrix of all lattice points
  trialpts=np.array([a.flatten() for a in np.meshgrid(xcoords,ycoords)]).T
  #If there are no fixed circles, all of the lattice points are valid
  if len(fixed)==0:
    validcens=trialpts
  else:
    #Matrix of user-specified inclusions (not necessarily within the bounding box)
    fixedcens=np.array([c[1:3] for c in fixed])
    #Radius matrix for fixed inclusions
    fixedrads=np.array([c[-1] for c in fixed])
    #Matrix of center-center distances
    distmat=scipy.spatial.distance_matrix(trialpts,fixedcens,p=2)
    #Matrix of surface-surface seperation distances
    #(negative values mean circles overlap)
    sepmat=distmat-fixedrads-incrad
    #Minimum separation distance from each lattice circle to a fixed inclusion
    minseparations=sepmat.min(axis=1)
    #Inclusions that meet the separation criteria
    validcens=[trialpts[idx,:] for idx in range(trialpts.shape[0]) if minseparations[idx]>=minsep]
  #From the list of points, generate a list of inclusions
  generated_inclusions=[]
  for idx in range(len(validcens)):
    this_inclusion = (idstart + idx*idstep,)
    this_inclusion += tuple(validcens[idx])
    this_inclusion += (incrad,)
    generated_inclusions.append(this_inclusion)
  #Done
  return generated_inclusions

def gen_perturbation_2D(oldpt,stdev):
  """Create a new trial position for a given point from a random perturbation

  Arguments:

    - oldpt = coordinates tuple, (x,y) as floats
    - stdev = standard deviation magnitude, as float

  Returns:

    - newpt = coordinates tuple, (x,y) as float
  """
  #Select random direction and size
  #Direction is random in polar coordinates
  phi = np.random.random()*2*np.pi #Angle in interval [0, 2*pi)
  step = np.random.normal(0,stdev) #Signed step-size
  #Convert to Cartesian coordinates
  dx = step*np.cos(phi)
  dy = step*np.sin(phi)
  #Add deltas to original coordinates
  oldx,oldy=oldpt
  newpt = (oldx + dx, oldy + dy)
  #Done
  return newpt

def check_perturbation(newpt,bbox,buffer,static,moving,inc_idx):
  """Check the trial position of a point against all collisions

  Note: this is a brute force check, which is not very efficient

  Arguments:

    - newpt = trial position
    - bbox = bounding box (xmin, ymin, xmax, ymax)
    - buffer = minimum surface separation distance, as float
    - static = list of fixed inclusions
    - moving = list of moving inclusions
    - inc_idx = index of the perturbed inclusion in the moving inclusions list

  Returns:

    - valid = boolean, True if change is OK, False if not
  """
  xmin,ymin,xmax,ymax=bbox
  nx,ny=newpt
  myrad=moving[inc_idx][3]
  #Check against bounding box
  valid = (nx > xmin + myrad + buffer) and (nx < xmax - myrad - buffer)
  valid = valid and (ny > ymin + myrad + buffer) and (ny < ymax - myrad - buffer)
  #Check against the other inclusions
  if valid:
    total_checklist = static + [t for idx,t in enumerate(moving) if idx != inc_idx]
    for incid,cx,cy,incrad in total_checklist:
      cendist = la.norm([nx-cx,ny-cy])
      valid = (cendist - buffer - incrad - myrad) >= 0.0
      if not valid:
        break
  #Done
  return valid


def perturb_inclusions(to_perturb,bbox,incrad,minsep,fixed,perturbation_rounds,perturbation_stdev):
  """Apply random perturbations to selected inclusions

  Arguments:

    - to_perturb = list of inclusions to perturb,
      each inclusion a tuple as for ``fixed
    - bbox = bounding box for the inclusions (xmin,ymin,xmax,ymax), all floats.
      Note that minsep is applied to the bounding box edges,
      so no generated inclusion will actually touch the bounding box.
    - incrad = inclusion radius, float.
    - minsep = minimum separation distance between inclusion surfaces (not centers)
    - fixed = list of fixed inclusions that may (or may not) be within the bounding box,
      each inclusion a tuple (ID, center x, center y, radius).
    - perturbation_rounds = Number of times to try perturbing the position of each inclusion, as integer
    - perturbation_stdev = Standard deviation of the perturbation magnitude, as float
 
  Returns:

    - generated_inclusions = list of generated inclusions,
      each inclusion a tuple as for ``fixed``
  """
  #Perform the prescibed number of rounds
  for pround in range(perturbation_rounds):
    #Loop over the inclusions to perturb
    for inc_idx,this_inc in enumerate(to_perturb):
      incid,oldx,oldy,radval = this_inc
      #Generate peturbed position
      oldpt = (oldx,oldy)
      newpt = gen_perturbation_2D(oldpt, perturbation_stdev)
      #Check for collision
      accepted = check_perturbation(
        newpt,bbox,minsep,fixed,to_perturb,inc_idx
      )
      #If trial position is accepted, update
      if accepted:
        newtup = (incid,)+newpt+(radval,)
        to_perturb[inc_idx]=newtup
  #Done
  return to_perturb
