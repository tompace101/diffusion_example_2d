"Customization of mesh"

#Standard library

#Site packages

#From simproc

#This directory
import lattice_inclusions_2D

#Constants
mesh_direct_fields=['S','d','H','slot_length',
                    'mcar1','mcar2','mcar3','mcar4']

#Functions

def mesh_get_template_input(self):
  """Compute the values that go into the mesh template, from the relevant inputs"""
  #Direct copy and initialization
  output={k:self.data[k] for k in mesh_direct_fields}
  #Start the inclusion list from the manual inclusions
  inclusionlist = self.data['inclusionlist']
  #Compute lattice inclusions
  if 'lattice' in self.data.keys():
    lattice_params=self.data['lattice']
    #Generate the list of boxes for the lattice inclusion
    #(xmin,ymin,xmax,ymax)
    sl=self.data["slot_length"]
    d=self.data["d"]
    boxes = [
      (-sl/2.0, -d/2.0, 0,      d/2.0),
      (0,       -d/2.0, sl/2.0, d/2.0)
    ]
    #Generate perturbed lattice inclusions
    idstart=lattice_params['inc_id_start']
    inc_id_step=lattice_params['inc_id_step']
    for bbox in boxes:
      #Call the lattice inclusion generation code for each box
      generated_inclusions = lattice_inclusions_2D.generate_lattice_inclusions(
        bbox,lattice_params['incrad'],lattice_params['minsep'],inclusionlist,idstart,inc_id_step
      )
      if len(generated_inclusions)>0:
        #Perturb the lattice-generated inclusions (not the manually entered ones)
        generated_inclusions = lattice_inclusions_2D.perturb_inclusions(
          generated_inclusions,
          bbox,lattice_params['incrad'],lattice_params['minsep'],inclusionlist,
          lattice_params['perturbation_rounds'],lattice_params['perturbation_stdev']
        )
        #Update the starting id
        lastid=generated_inclusions[-1][0]
        idstart=lastid+inc_id_step
        #Add these generated inclusions to the overall list
        inclusionlist += generated_inclusions
  #Add inclusionlist to the output
  output['inclusionlist'] = inclusionlist
  #Split inclusions into parts left and right of pore center
  left_inclusions=[t for t in inclusionlist if t[1] < 0.0]
  right_inclusions=[t for t in inclusionlist if t[1] > 0.0]
  #Lists of inclusion IDs
  output['left_ids']=", ".join(['%d'%t[0] for t in left_inclusions])
  output['right_ids']=", ".join(['%d'%t[0] for t in right_inclusions])
  #Done
  return output


#List of functions to be bound as methods
request_methods=[mesh_get_template_input]
