"""Post-processing calculations"""

import numpy as np
import fenics as fem

def calc_delta(self, vmin, vmax, outattr):
  """Compute a delta from min and max

  Arguments:
  
    - vmin = minimum value
    - vmax = maximum value
    - outattr = attribute path for storing result"""
  dv=self.get_stored(vmax)-self.get_stored(vmin)
  self.set_nested(outattr,dv)
  return

def calc_ratio(self, numerator, denominator, outattr):
  a=self.get_stored(numerator)
  b=self.get_stored(denominator)
  res=a/b
  self.set_nested(outattr, res)

def effective_D(self,outattr,fluxattr,areaattr,start_conc_attr,end_conc_attr,delta_s_attr):
  """Calculate effective diffusion constant, using concentration averages rather than point values

  Arguments:

    - outattr = attribute path for storage of result
    - fluxattr = attribute path to previously calculated total

        This requires a previous call to fluxintegral (or similar).

    - areaattr = attribute path to previously calculated area in results dictionary

        This requires a previous call to facet_area (or similar).

    - start_conc_attr = attribute path to concentration value at starting boundary
    - end_conc_attr = attribute path to concentration value at ending boundary
    - delta_s_attr = attribute path to value of Delta s for effective D calculation

  No return value.
  No output files."""
  #Get the values from attributes
  totflux=self.get_nested(fluxattr)
  area=self.get_nested(areaattr)
  startconc=self.get_nested(start_conc_attr)
  endconc=self.get_nested(end_conc_attr)
  delta_s=self.get_nested(delta_s_attr)
  #Calculate the change in concentration between the two points
  delta_c=endconc-startconc
  #Calculate diffusion constant
  Deff=float(totflux/area*delta_s/delta_c)
  #Store result
  self.set_nested(outattr,Deff)
  return

def free_volume_fraction(self,outattr,pcells,dimensions):
  """Compute the free volume fraction of the specified volume.

  Arguments:

    - outattr = attribute path for storage of result
    - pcells = sequence of physical cell numbers to get volumes of
    - dimensions = sequence of dimension values (or Stored) to multiply to get the theoretical volume

  No return values.
  No output files.
  """
  #Get the theoretical volume
  dim_vals = [self.get_stored(d) for d in dimensions]
  theovol = np.prod(dim_vals)
  #Get the actual volume
  this_dx=fem.Measure('cell',domain=self.meshinfo.mesh,subdomain_data=self.meshinfo.cells)
  actvol = 0.0
  for pc in pcells:
    calcvol=fem.assemble(fem.Constant(1)*this_dx(pc))
    actvol += calcvol
  #Take the ratio
  phi = float(actvol / theovol)
  #Store result
  self.set_nested(outattr,phi)
  return


#List of functions to be bound as methods
request_methods=[calc_ratio, calc_delta, effective_D, free_volume_fraction]